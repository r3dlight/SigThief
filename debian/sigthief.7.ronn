SigThief(7) -- Take a Signature from a binary and add it to another binary

====

#SYNOPSIS
`sigthief` [Usage]: sigthief [-h] [-i FILE] [-r] [-a] [-o OUTPUTFILE] [-s SIGFILE] [-t TARGETFILE] [-c] [-T]

## DESCRIPTION

SigThief rips a signature off a signed PE file and append it 
to another one, fixing up the certificate table to sign the file.


   * `-h`, `--help`
     Show this help message and exit

   * `-i` FILE, `--file=FILE`
     Input file

   * `-r`, `--rip`
     Rip signature off inputfile

   * `-a`, `--add`
      Add signautre to targetfile

   * `-o OUTPUTFILE`, `--output=OUTPUTFILE`
     Output file

   * `-s SIGFILE`, `--sig=SIGFILE`
     Binary signature from disk

   * `-t TARGETFILE`, `--target=TARGETFILE`
     File to append signature too

   * `-c`, `--checksig`
     File to check if signed; does not verify signature

   * `-T`, `--truncate`
     Truncate signature (i.e. remove sig)


## EXAMPLES

* Take a Signature from a binary and add it to another binary: 
$ sigthief -i tcpview.exe -t x86_meterpreter_stager.exe -o /tmp/msftesting_tcpview.exe 

* Save Signature to disk for use later: 
$ sigthief -i tcpview.exe -r

* Use the ripped signature: 
$ sigthief -s tcpview.exe_sig -t x86_meterpreter_stager.exe

* Truncate (remove) signature: 
$ sigthief -i tcpview.exe -T

* Check if there is a signature (does not check validity): 
$ sigthief -i tcpview.exe -c


##SEE ALSO

-  [ On github](https://github.com/secretsquirrel/SigThief)

## MANPAGE AUTHOR

Stephane Neveu <stefneveu@gmail.com>


